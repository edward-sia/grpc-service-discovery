package com.edwardskt.propertyservice.domain

data class PropertyDto(
        val id: Int,
        val name: String,
        val price: Double,
        val suburb: String,
        val status: String
)
