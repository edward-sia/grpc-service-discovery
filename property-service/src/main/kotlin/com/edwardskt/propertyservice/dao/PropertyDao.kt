package com.edwardskt.propertyservice.dao

import com.edwardskt.propertyservice.domain.PropertyDto

interface PropertyDao {
    fun getProperties(): List<PropertyDto>
}
