package com.edwardskt.propertyservice.dao.fake

import com.edwardskt.propertyservice.dao.PropertyDao
import com.edwardskt.propertyservice.domain.PropertyDto
import org.springframework.stereotype.Repository

@Repository
class FakePropertyDao: PropertyDao {
    override fun getProperties(): List<PropertyDto> {
        println("Hit server")
        return listOf(PropertyDto(1, "2/30 Jackson St Croydon 3136", 600_000.00, "Croydon", "SOLD"),
                PropertyDto(2, "10/1440 Whitehorse Rd Mitcham, 3132", 700_000.00, "Mitcham", "SOLD"))
    }
}
