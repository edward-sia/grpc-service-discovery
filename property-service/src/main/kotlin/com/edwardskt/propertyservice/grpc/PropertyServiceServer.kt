package com.edwardskt.propertyservice.grpc

import com.edwardskt.grpc.ListPropertyRequest
import com.edwardskt.grpc.ListPropertyResponse
import com.edwardskt.grpc.PropertyGrpc
import com.edwardskt.grpc.PropertyResponse
import com.edwardskt.propertyservice.dao.PropertyDao
import io.grpc.StatusRuntimeException
import io.grpc.stub.StreamObserver
import net.devh.boot.grpc.server.service.GrpcService
import java.util.stream.Collectors

@GrpcService
class PropertyServiceServer(
        private val propertyDao: PropertyDao
): PropertyGrpc.PropertyImplBase() {
    override fun listProperty(request: ListPropertyRequest, responseObserver: StreamObserver<ListPropertyResponse>) {

        try {
            val response = ListPropertyResponse.newBuilder()
                    .addAllResults(propertyDao.getProperties().stream()
                            .map {
                                PropertyResponse.newBuilder()
                                        .setId(it.id)
                                        .setName(it.name)
                                        .setPrice(it.price)
                                        .setStatus(it.status)
                                        .setSuburb(it.suburb)
                                        .build()
                            }
                            .collect(Collectors.toList())
                    )
                    .build()
            responseObserver.onNext(response)
            responseObserver.onCompleted()
        }
        catch (ex: StatusRuntimeException) {
            responseObserver.onError(ex)
        }
    }
}
