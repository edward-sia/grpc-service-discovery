package com.edwardskt.propertyapi.domain.response

data class PropertiesResponse(
        val properties: List<PropertyResponse?>
)
