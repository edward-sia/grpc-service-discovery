package com.edwardskt.propertyapi.domain.response

data class PropertyResponse(
        val id: Int,
        val name: String,
        val price: Double,
        val suburb: String,
        val status: String
)
