package com.edwardskt.propertyapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan
class PropertyApiApplication {

	companion object {
		@JvmStatic
		fun main(args: Array<String>) {
			runApplication<PropertyApiApplication>(*args)
		}
	}
}
