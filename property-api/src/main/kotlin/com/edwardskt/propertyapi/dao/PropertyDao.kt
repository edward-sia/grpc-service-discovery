package com.edwardskt.propertyapi.dao

import com.edwardskt.propertyapi.domain.PropertyDto

interface PropertyDao {
    fun listProperty(): List<PropertyDto>
}
