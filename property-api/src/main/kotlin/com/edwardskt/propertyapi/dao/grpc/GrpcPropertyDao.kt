package com.edwardskt.propertyapi.dao.grpc

import com.edwardskt.grpc.ListPropertyRequest
import com.edwardskt.grpc.PropertyGrpc
import com.edwardskt.grpc.PropertyResponse
import com.edwardskt.propertyapi.dao.PropertyDao
import com.edwardskt.propertyapi.domain.PropertyDto
import net.devh.boot.grpc.client.inject.GrpcClient
import org.springframework.stereotype.Component
import java.util.stream.Collectors

@Component("grpcPropertyDao")
class GrpcPropertyDao: PropertyDao {

    @GrpcClient(value = "property-service")
    lateinit var propertyStub: PropertyGrpc.PropertyBlockingStub

    override fun listProperty(): List<PropertyDto> {
        println("try to hit server")
        val list = propertyStub.listProperty(ListPropertyRequest.getDefaultInstance())
                .resultsList
                .stream()
                .map(this::toDto)
                .collect(Collectors.toList())

        return list
    }

    private fun toDto(resp: PropertyResponse): PropertyDto {
        return PropertyDto(resp.id, resp.name, resp.price, resp.suburb, resp.status)
    }

}
