package com.edwardskt.propertyapi.controller

import com.edwardskt.propertyapi.dao.PropertyDao
import com.edwardskt.propertyapi.domain.response.PropertiesResponse
import com.edwardskt.propertyapi.domain.response.PropertyResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/properties")
class PropertyController{

    @Autowired
    lateinit var propertyDao: PropertyDao

    @GetMapping
    fun getProperties(): ResponseEntity<PropertiesResponse> {
        val resp = propertyDao.listProperty()
                .map { propertyDto -> PropertyResponse(
                        id = propertyDto.id,
                        name = propertyDto.name,
                        price = propertyDto.price,
                        status = propertyDto.status,
                        suburb = propertyDto.suburb
                ) }
                .toCollection(arrayListOf())
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(PropertiesResponse(resp))
    }
}
