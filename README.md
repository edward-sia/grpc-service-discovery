# GRPC and Consul as Service Discovery

## Description

Sample Demonstration of microservices interaction via Consul Service Discovery

---

## Architecture

### Microservices

#### property-service

Implementation of `internal microservice` that returns property(ies) via GRPC

#### property-api

Implementation of `public-facing api (microservice)` that translates external HTTP requests into internal GRPC client calls.

---

## Installation

Install both property-service and property-api

    mvn install

---

## Run

Start up both services

    mvn spring-boot:run

Hit browser

    http://localhost:8080/properties/

---

## Out of Scope

- Protocol buffer files are duplicated across services. Ideally this should be published and shared across using artifact repo like Nexus.

- Containerization
